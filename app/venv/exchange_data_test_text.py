import sys, hello_world

def main():
    messages = sys.stdin.readlines()

    object = hello_world.HelloWorld("Hello, ", messages[0])

    print (object.name)
    sys.stdout.flush()

if __name__ == '__main__':
    main()