import sys, json, hello_world

def read_in():
    messages = sys.stdin.readlines()
    return json.loads(messages[0])

def main():
    message = read_in()

    object = hello_world.HelloWorld("Hello, ", message["name"])

    jsonResponse = {
        "greeting": object.greeting,
        "name": object.name
    }
    messageOut = json.dumps(jsonResponse)

    print(messageOut)
    sys.stdout.flush()

if __name__ == '__main__':
    main()