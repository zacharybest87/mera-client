function changeByPythonByText() {
    let {PythonShell} = require('python-shell');

    let options = {
	  mode: 'text',
	  //pythonPath: '',
	  pythonOptions: ['-u'], // get print results in real-time
	  scriptPath: './app/venv/',
	  args: ['value1', 'value2', 'value3']
	};

    let pyshell = new PythonShell('exchange_data_test_text.py', options);

    let messageIn = document.getElementById('name').innerText;

    pyshell.send(messageIn);

	pyshell.on('message', function (message) {
		if (message != null) {
			console.log(message)
			document.getElementById('demo').innerHTML = "Hello, " + messageIn;
		} else {
			document.getElementById('demo').innerHTML = "There was an error in Python response.";
		}
	});

	// end the input stream and allow the process to exit
	pyshell.end(function (err,code,signal) {
	  if (err) throw err;
	  console.log('The exit code was: ' + code);
	  console.log('The exit signal was: ' + signal);
	  console.log('finished');
	});
}

function changeByPythonByJson() {
    let {PythonShell} = require('python-shell');

    let options = {
	  mode: 'json',
	  //pythonPath: '',
	  pythonOptions: ['-u'], // get print results in real-time
	  scriptPath: './app/venv/',
	  args: ['value1', 'value2', 'value3']
	};

    let pyshell = new PythonShell('exchange_data_test_json.py', options);

    let messageIn = document.getElementById('name').innerHTML;

	let jsonMessageIn = {"name": messageIn};

	pyshell.send(jsonMessageIn);

	pyshell.on('message', function (message) {
		console.log(message)
	  	document.getElementById('demo').innerHTML = message.greeting + message.name;
	});

	// end the input stream and allow the process to exit
	pyshell.end(function (err,code,signal) {
	  if (err) throw err;

	  console.log('The exit code was: ' + code);
	  console.log('The exit signal was: ' + signal);
	  console.log('finished');
	});
}