import ctypes


class MouseButton:
    """
    A mouse button

    This class provides a convenient way to access
    buttonDown and buttonUp mouseEvents
    """
    def __init__(self, buttonDownEvent, buttonUpEvent, dwData=0):
        self.buttonDownEvent = buttonDownEvent
        self.buttonUpEvent = buttonUpEvent
        self.dwData = dwData

    def _doEvent(self, event):
        ctypes.windll.user32.mouse_event(event, 0, 0, self.dwData, 0)
        
    def holdKeyDown(self):
        self._doEvent(self.buttonDownEvent)

    def releaseKey(self):
        self._doEvent(self.buttonUpEvent)

    def click(self):
        self.holdKeyDown()
        self.releaseKey()
        
class Mouse:
    """
    The systems mouse

    This class provides allows the mouse to be controlled
    programmatically.
    """
    MOUSEEVENTF_LEFTDOWN = 0x0002
    MOUSEEVENTF_LEFTUP = 0x0004

    MOUSEEVENTF_RIGHTDOWN = 0x0008
    MOUSEEVENTF_RIGHTUP = 0x0010
    
    MOUSEEVENTF_MIDDLEDOWN = 0x0020
    MOUSEEVENTF_MIDDLEUP = 0x0040

    XBUTTON1 = 0x0001
    XBUTTON2 = 0x0002
    MOUSEEVENTF_XDOWN = 0x0080
    MOUSEEVENTF_XUP = 0x0100

    leftMouseButton = MouseButton(MOUSEEVENTF_LEFTDOWN, MOUSEEVENTF_LEFTUP)
    rightMouseButton = MouseButton(MOUSEEVENTF_RIGHTDOWN, MOUSEEVENTF_RIGHTUP)
    middleMouseButton = MouseButton(MOUSEEVENTF_MIDDLEDOWN, MOUSEEVENTF_MIDDLEUP)
    xbutton1MouseButton = MouseButton(MOUSEEVENTF_XDOWN, MOUSEEVENTF_XUP, XBUTTON1)
    xbutton2MouseButton = MouseButton(MOUSEEVENTF_XDOWN, MOUSEEVENTF_XUP, XBUTTON2)
    buttons = {
        "left" : leftMouseButton,
        "right" : rightMouseButton,
        "middle" : middleMouseButton,
        "xbutton1" : xbutton1MouseButton,
        "xbutton2" : xbutton2MouseButton
    }
    
    def move(x, y):
        ctypes.windll.user32.SetCursorPos(x, y)

    def click(button="left"):
        Mouse.buttons[button].click()

    def clickPos(x, y, button="left"):
        Mouse.moveMouse(x, y)
        Mouse.click()

    def clickPoint(point, button="left"):
        Mouse.move(point[0], point[1])
        Mouse.click()
        
